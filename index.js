
'use strict';


const Obj = {
    isObject(obj) {
        return typeof obj === 'object' && obj !== null && !Array.isArray(obj)
    },

    has(obj, prop) {
        return typeof obj === 'object' ? Object.prototype.hasOwnProperty.call(obj, prop) : false
    },
    isEnumerable(obj, prop) {
        return typeof obj === 'object' ? Object.prototype.propIsEnumerable.call(obj, prop) : false
    },

    deepAssign(target) {
        const self = this
        function toObject(val) {
            if (val === null || val === undefined) throw new TypeError('Cannot convert undefined or null to object')
            return Object(val)
        }

        function assignKey(to, from, key) {
            var val = from[key]
            if (val === undefined || val === null) return;

            if (self.has(to, key)) {
                if (to[key] === undefined || to[key] === null) throw new TypeError('Cannot convert undefined or null to object (' + key + ')')
            }

            if (!self.has(to, key) || !self.isObject(val)) {
                to[key] = val;
            } else {
                to[key] = assign(Object(to[key]), from[key]);
            }
        }

        function assign(to, from) {
            if (to === from) {
                return to;
            }

            from = Object(from);

            for (var key in from) {
                if (self.has(from, key)) {
                    assignKey(to, from, key);
                }
            }

            if (Object.getOwnPropertySymbols) {
                var symbols = Object.getOwnPropertySymbols(from);

                for (var i = 0; i < symbols.length; i++) {
                    if (self.isEnumerable(from, symbols[i])) {
                        assignKey(to, from, symbols[i]);
                    }
                }
            }

            return to;
        }

        target = toObject(target);

        for (var s = 1; s < arguments.length; s++) {
            assign(target, arguments[s]);
        }

        return target;
    },


    limit(obj, propList, dflt = undefined) {
        let nobj = {}
        propList = Array.isArray(propList) ? propList : propList ? [propList] : []
        if (!Arr.empty(propList)) {
            propList.forEach(key => {
                if (Obj.has(obj, key)) {
                    nobj[key] = obj[key]
                } else if (dflt) {
                    nobj[key] = dflt
                }
            })
        }
        return nobj
    },

    except(obj, propList) {
        let nobj = {}
        propList = Array.isArray(propList) ? propList : propList ? [propList] : []
        for (let prop in obj) {
            if (!~propList.indexOf(prop)) {
                nobj[prop] = obj[prop]
            }
        }
        return nobj
    },

    isEmpty(obj) {
        return Object.keys(obj).length <= 0
    },

    trimEmptyProps(obj, test = v => !!v) {
        let nobj = {}
        if (typeof obj !== 'object' || obj === null) return nobj
        for (let key in obj) {
            if (test(obj[key])) {
                nobj[key] = obj[key]
            }
        }
        return nobj
    },

    testForAnyProps(obj, propList) {
        propList = Arr.toArray(propList)
        let nobj = this.limit(obj, propList, false)
        return this.propCount(nobj) > 0
    },

    testForAllProps(obj, propList) {
        propList = Arr.toArray(propList)
        let nobj = this.limit(obj, propList, false)
        return this.propCount(nobj) === propList.length
    },

    propCount(obj) {
        return typeof obj !== 'object' || obj === null ? 0 : Object.keys(obj).length
    },

    removeProps(obj, propList) {
        if (!this.isObject(obj)) return {}
        propList = Arr.toArray(propList)
        let nobj = {}
        for (let key in obj) {
            if (propList.indexOf(key) < 0) nobj[key] = obj[key]
        }
        return nobj
    },

    getProps(obj) {
        let props = []
        for (let prop in obj) {
            if (Obj.has(obj, prop)) {
                props.push(prop)
            }
        }
        return props
    },

    toArray(obj) {
        return Obj.getProps(obj).map(prop => obj[prop])
    }
}

module.exports = Obj
